package ru.t1.ktubaltseva.tm;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.t1.ktubaltseva.tm.unit.controller.ProjectControllerTest;
import ru.t1.ktubaltseva.tm.unit.controller.ProjectsControllerTest;
import ru.t1.ktubaltseva.tm.unit.controller.TaskControllerTest;
import ru.t1.ktubaltseva.tm.unit.controller.TasksControllerTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ProjectControllerTest.class, TaskControllerTest.class, ProjectsControllerTest.class, TasksControllerTest.class,})
public class SuiteController {

}