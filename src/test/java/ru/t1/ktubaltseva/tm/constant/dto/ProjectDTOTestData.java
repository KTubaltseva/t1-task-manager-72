package ru.t1.ktubaltseva.tm.constant.dto;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class ProjectDTOTestData {

    @Nullable
    public final static ProjectDTO NULL_MODEL = null;

    @Nullable
    public final static String NULL_MODEL_ID = null;

    @Nullable
    public final static String NON_EXISTENT_MODEL_ID = "NON_EXISTENT_MODEL_ID";

    @Nullable
    public final static ProjectDTO NON_EXISTENT_MODEL = new ProjectDTO();

    @NotNull
    public final static ProjectDTO MODEL_1 = new ProjectDTO("name1");

    @NotNull
    public final static ProjectDTO MODEL_2 = new ProjectDTO("name2");

    @NotNull
    public final static List<ProjectDTO> MODEL_LIST = Arrays.asList(MODEL_1, MODEL_2);

    @Nullable
    public final static String MODEL_NAME = "MODEL_NAME";

}
