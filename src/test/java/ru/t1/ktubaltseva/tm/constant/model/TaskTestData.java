package ru.t1.ktubaltseva.tm.constant.model;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class TaskTestData {

    @Nullable
    public final static Task NULL_MODEL = null;

    @Nullable
    public final static String NULL_MODEL_ID = null;

    @Nullable
    public final static String NON_EXISTENT_MODEL_ID = "NON_EXISTENT_MODEL_ID";

    @Nullable
    public final static Task NON_EXISTENT_MODEL = new Task();

    @NotNull
    public final static Task MODEL_1 = new Task("name1");

    @NotNull
    public final static Task MODEL_2 = new Task("name2");

    @NotNull
    public final static Project PROJECT_1 = new Project("name1");

    @NotNull
    public final static List<Task> MODEL_LIST = Arrays.asList(MODEL_1, MODEL_2);

    @Nullable
    public final static String MODEL_NAME = "MODEL_NAME";

}
