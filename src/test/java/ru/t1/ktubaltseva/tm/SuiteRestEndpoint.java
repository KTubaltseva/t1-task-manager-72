package ru.t1.ktubaltseva.tm;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.t1.ktubaltseva.tm.unit.endpoint.AuthRestEndpointTest;
import ru.t1.ktubaltseva.tm.unit.endpoint.ProjectRestEndpointTest;
import ru.t1.ktubaltseva.tm.unit.endpoint.TaskRestEndpointTest;
import ru.t1.ktubaltseva.tm.unit.endpoint.UserRestEndpointTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({AuthRestEndpointTest.class, ProjectRestEndpointTest.class, TaskRestEndpointTest.class, UserRestEndpointTest.class})
public class SuiteRestEndpoint {

}