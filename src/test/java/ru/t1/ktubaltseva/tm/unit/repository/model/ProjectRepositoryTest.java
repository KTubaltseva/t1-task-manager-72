package ru.t1.ktubaltseva.tm.unit.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.model.UserRepository;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

import static ru.t1.ktubaltseva.tm.constant.model.ProjectTestData.*;
import static ru.t1.ktubaltseva.tm.constant.model.UserTestData.TEST_USER_LOGIN;
import static ru.t1.ktubaltseva.tm.constant.model.UserTestData.TEST_USER_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class ProjectRepositoryTest {

    @NotNull
    private final Project projectWithUser = MODEL_1;

    @NotNull
    private final Project projectWithoutUser = MODEL_2;

    @Nullable
    private User testUser;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        testUser = userRepository.findById(UserUtil.getUserId()).get();
        projectWithUser.setUser(testUser);

        repository.saveAndFlush(projectWithUser);
        repository.saveAndFlush(projectWithoutUser);
    }

    @After
    @SneakyThrows
    public void after() {
        repository.deleteAll();
    }

    @Test
    @SneakyThrows
    public void countByUser() {
        Assert.assertEquals(1, repository.countByUser(testUser));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteAllByUser() {
        Assert.assertEquals(1, repository.countByUser(testUser));
        repository.deleteAllByUser(testUser);
        Assert.assertEquals(0, repository.countByUser(testUser));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteByUserAndId() {
        Assert.assertEquals(1, repository.countByUser(testUser));
        repository.deleteByUserAndId(testUser, projectWithUser.getId());
        Assert.assertEquals(0, repository.countByUser(testUser));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    public void existsByUserAndId() {
        Assert.assertTrue(repository.existsByUserAndId(testUser, projectWithUser.getId()));
        Assert.assertFalse(repository.existsByUserAndId(testUser, NON_EXISTENT_MODEL_ID));
        Assert.assertFalse(repository.existsByUserAndId(testUser, projectWithoutUser.getId()));
    }

    @Test
    @SneakyThrows
    public void findAllByUser() {
        @NotNull final List<Project> projects = repository.findAllByUser(testUser);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    @SneakyThrows
    public void findByUserAndId() {
        Assert.assertTrue(repository.findByUserAndId(testUser, projectWithUser.getId()).isPresent());
        Assert.assertFalse(repository.findByUserAndId(testUser, projectWithoutUser.getId()).isPresent());
    }

}
