package ru.t1.ktubaltseva.tm.unit.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.repository.dto.UserDTORepository;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import static ru.t1.ktubaltseva.tm.constant.dto.TaskDTOTestData.*;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.TEST_USER_LOGIN;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.TEST_USER_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class ProjectTaskDTOServiceTest {

    @NotNull
    private final TaskDTO taskWithUser = MODEL_1;

    @NotNull
    private final ProjectDTO project = PROJECT_1;

    @Nullable
    private UserDTO testUser;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectTaskDTOService service;

    @NotNull
    @Autowired
    private UserDTORepository userRepository;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        testUser = userRepository.findById(UserUtil.getUserId()).get();

        taskWithUser.setProjectId(project.getId());

        projectService.add(UserUtil.getUserId(), project);

        taskService.add(testUser.getId(), taskWithUser);
    }

    @After
    @SneakyThrows
    public void after() {
        taskService.clear();
        projectService.clear();
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteByUserIdAndProjectId() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.deleteByUserIdAndProjectId(NULL_USER_ID, project.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.deleteByUserIdAndProjectId(testUser.getId(), NULL_PROJECT_ID));

        service.deleteByUserIdAndProjectId(testUser.getId(), project.getId());
        Assert.assertEquals(0, taskService.findAllByUserIdAndProjectId(testUser.getId(), project.getId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteAllByUserId() {
        Assert.assertThrows(UserNotFoundException.class, () -> service.deleteAllByUserId(NULL_USER_ID));

        service.deleteAllByUserId(testUser.getId());
        Assert.assertEquals(0, taskService.findAllByUserIdAndProjectId(testUser.getId(), project.getId()).size());
    }

}
