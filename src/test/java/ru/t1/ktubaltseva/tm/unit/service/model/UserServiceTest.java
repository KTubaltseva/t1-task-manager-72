package ru.t1.ktubaltseva.tm.unit.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.IUserService;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.ArrayList;
import java.util.List;

import static ru.t1.ktubaltseva.tm.constant.model.UserTestData.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class UserServiceTest {

    @NotNull
    private final User user = new User();

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IUserService service;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        user.setLogin(USER_1_LOGIN);
        user.setPasswordHash(passwordEncoder.encode(USER_1_PASSWORD));
        user.setEmail(USER_1_EMAIL);

        service.add(user);
    }

    @After
    @SneakyThrows
    public void after() {
        try {
            service.delete(user);
        } catch (Exception e) {
        }
    }

    @Test
    @SneakyThrows
    @Transactional
    public void create() {
        Assert.assertEquals(3, service.count());
        @NotNull final User user = service.create();
        Assert.assertEquals(4, service.count());

        service.delete(user);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void add() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(NULL_USER));

        Assert.assertEquals(3, service.count());
        @NotNull final User user = new User();

        this.user.setLogin(USER_LOGIN);
        this.user.setPasswordHash(passwordEncoder.encode(USER_PASSWORD));
        this.user.setEmail(USER_EMAIL);

        service.add(user);
        Assert.assertEquals(4, service.count());

        service.delete(user);
    }

    @Test
    @SneakyThrows
    @Transactional
    public void create_list() {
        service.delete(user);

        Assert.assertEquals(2, service.count());

        @NotNull final List<User> users = new ArrayList<User>();
        users.add(user);
        service.create(users);

        Assert.assertEquals(3, service.count());
    }

    @Test
    @Ignore
    @SneakyThrows
    @Transactional
    public void clear() {
        Assert.assertEquals(2, service.count());
        service.clear();
        Assert.assertEquals(0, service.count());
    }

    @Test
    @SneakyThrows
    public void existsById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_USER_ID));

        Assert.assertTrue(service.existsById(user.getId()));
        Assert.assertFalse(service.existsById(NON_EXISTENT_USER_ID));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final List<User> users = service.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(3, users.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.findById(NULL_USER_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(NON_EXISTENT_USER_ID));

        Assert.assertNotNull(service.findById(user.getId()));
        Assert.assertEquals(user.getId(), service.findById(user.getId()).getId());
    }

    @Test
    @SneakyThrows
    public void count() {
        Assert.assertEquals(3, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void delete() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(NULL_USER));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.delete(NON_EXISTENT_USER));

        Assert.assertEquals(3, service.count());
        service.delete(user);
        Assert.assertEquals(2, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteById() {
        Assert.assertThrows(IdEmptyException.class, () -> service.deleteById(NULL_USER_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.deleteById(NON_EXISTENT_USER_ID));

        Assert.assertEquals(3, service.count());
        service.deleteById(user.getId());
        Assert.assertEquals(2, service.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void update() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NULL_USER));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.update(NON_EXISTENT_USER));

        user.setLogin(USER_LOGIN);
        service.update(user);
        Assert.assertEquals(USER_LOGIN, service.findById(user.getId()).getLogin());
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> service.findByLogin(NULL_LOGIN));

        Assert.assertNotNull(service.findByLogin(user.getLogin()));
        Assert.assertNull(service.findByLogin(NON_EXISTENT_USER_LOGIN));
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> service.findByEmail(NULL_EMAIL));

        Assert.assertNotNull(service.findByEmail(user.getEmail()));
        Assert.assertNull(service.findByEmail(NON_EXISTENT_USER_EMAIL));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> service.removeByLogin(NULL_LOGIN));

        service.removeByLogin(user.getLogin());
        Assert.assertFalse(service.isLoginExists(user.getLogin()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void removeByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> service.removeByEmail(NULL_EMAIL));

        service.removeByEmail(user.getEmail());
        Assert.assertFalse(service.isEmailExists(user.getEmail()));
    }

    @Test
    @SneakyThrows
    public void isLoginExists() {
        Assert.assertThrows(LoginEmptyException.class, () -> service.isLoginExists(NULL_LOGIN));

        Assert.assertTrue(service.isLoginExists(user.getLogin()));
        Assert.assertFalse(service.isLoginExists(NON_EXISTENT_USER_LOGIN));
    }

    @Test
    @SneakyThrows
    public void isEmailExists() {
        Assert.assertThrows(EmailEmptyException.class, () -> service.isEmailExists(NULL_EMAIL));

        Assert.assertTrue(service.isEmailExists(user.getEmail()));
        Assert.assertFalse(service.isEmailExists(NON_EXISTENT_USER_EMAIL));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> service.lockUserByLogin(NULL_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> service.lockUserByLogin(NON_EXISTENT_USER_LOGIN));

        service.lockUserByLogin(user.getLogin());
        @NotNull final User user = service.findByLogin(this.user.getLogin());
        Assert.assertTrue(user.isLocked());
    }

}
