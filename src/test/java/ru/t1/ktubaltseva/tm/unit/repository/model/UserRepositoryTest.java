package ru.t1.ktubaltseva.tm.unit.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.UserRepository;

import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.*;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class UserRepositoryTest {

    @NotNull
    private final User user = new User();

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private UserRepository repository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;


    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        user.setLogin(USER_1_LOGIN);
        user.setPasswordHash(passwordEncoder.encode(USER_1_PASSWORD));
        user.setEmail(USER_1_EMAIL);

        repository.saveAndFlush(user);
    }

    @After
    @SneakyThrows
    public void after() {
        repository.delete(user);
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        Assert.assertTrue(repository.findByLogin(user.getLogin()).isPresent());
        Assert.assertFalse(repository.findByLogin(NON_EXISTENT_USER_LOGIN).isPresent());
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        Assert.assertTrue(repository.findByEmail(user.getEmail()).isPresent());
        Assert.assertFalse(repository.findByEmail(NON_EXISTENT_USER_EMAIL).isPresent());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteByLogin() {
        Assert.assertTrue(repository.findByLogin(user.getLogin()).isPresent());
        repository.deleteByLogin(user.getLogin());
        Assert.assertFalse(repository.findByLogin(user.getLogin()).isPresent());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteByEmail() {
        Assert.assertTrue(repository.findByEmail(user.getEmail()).isPresent());
        repository.deleteByEmail(user.getEmail());
        Assert.assertFalse(repository.findByLogin(user.getEmail()).isPresent());
    }

    @Test
    @SneakyThrows
    public void existsByLogin() {
        Assert.assertTrue(repository.existsByLogin(user.getLogin()));
        Assert.assertFalse(repository.existsByLogin(NON_EXISTENT_USER_LOGIN));
    }

    @Test
    @SneakyThrows
    public void existsByEmail() {
        Assert.assertTrue(repository.existsByEmail(user.getEmail()));
        Assert.assertFalse(repository.existsByEmail(NON_EXISTENT_USER_EMAIL));
    }

}
