package ru.t1.ktubaltseva.tm.unit.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.configuration.ApplicationConfiguration;
import ru.t1.ktubaltseva.tm.configuration.SecurityConfig;
import ru.t1.ktubaltseva.tm.dto.model.ProjectDTO;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.repository.dto.ProjectDTORepository;
import ru.t1.ktubaltseva.tm.repository.dto.TaskDTORepository;
import ru.t1.ktubaltseva.tm.util.UserUtil;

import java.util.List;

import static ru.t1.ktubaltseva.tm.constant.dto.TaskDTOTestData.*;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.TEST_USER_LOGIN;
import static ru.t1.ktubaltseva.tm.constant.dto.UserDTOTestData.TEST_USER_PASSWORD;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class, SecurityConfig.class})
public class TaskDTORepositoryTest {

    @NotNull
    private final TaskDTO taskWithUserWithProject = MODEL_1;

    @NotNull
    private final TaskDTO taskWithoutUserWithoutProject = MODEL_2;

    @NotNull
    private final ProjectDTO project = PROJECT_1;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private TaskDTORepository repository;

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        projectRepository.saveAndFlush(project);

        taskWithUserWithProject.setUserId(UserUtil.getUserId());
        taskWithUserWithProject.setProjectId(project.getId());

        repository.saveAndFlush(taskWithUserWithProject);
        repository.saveAndFlush(taskWithoutUserWithoutProject);
    }

    @After
    @SneakyThrows
    public void after() {
        repository.deleteAll();
        projectRepository.delete(project);
    }

    @Test
    @SneakyThrows
    public void countByUserId() {
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteAllByUserId() {
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
        repository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteByUserIdAndId() {
        Assert.assertEquals(1, repository.countByUserId(UserUtil.getUserId()));
        repository.deleteByUserIdAndId(UserUtil.getUserId(), taskWithUserWithProject.getId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
        Assert.assertEquals(1, repository.count());
    }

    @Test
    @SneakyThrows
    public void existsByUserIdAndId() {
        Assert.assertTrue(repository.existsByUserIdAndId(UserUtil.getUserId(), taskWithUserWithProject.getId()));
        Assert.assertFalse(repository.existsByUserIdAndId(UserUtil.getUserId(), NON_EXISTENT_MODEL_ID));
        Assert.assertFalse(repository.existsByUserIdAndId(UserUtil.getUserId(), taskWithoutUserWithoutProject.getId()));
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final List<TaskDTO> tasks = repository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndId() {
        Assert.assertTrue(repository.findByUserIdAndId(UserUtil.getUserId(), taskWithUserWithProject.getId()).isPresent());
        Assert.assertFalse(repository.findByUserIdAndId(UserUtil.getUserId(), taskWithoutUserWithoutProject.getId()).isPresent());
    }

    @Test
    @SneakyThrows
    public void countByUserIdAndProjectId() {
        Assert.assertEquals(1, repository.countByUserIdAndProjectId(UserUtil.getUserId(), project.getId()));
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdAndProjectId() {
        @NotNull final List<TaskDTO> tasks = repository.findAllByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    @SneakyThrows
    @Transactional
    public void deleteAllByUserIdAndProjectId() {
        Assert.assertEquals(1, repository.countByUserIdAndProjectId(UserUtil.getUserId(), project.getId()));
        repository.deleteAllByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
        Assert.assertEquals(0, repository.countByUserIdAndProjectId(UserUtil.getUserId(), project.getId()));
        Assert.assertEquals(1, repository.count());
    }

}
