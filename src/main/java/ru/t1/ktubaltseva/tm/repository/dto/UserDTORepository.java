package ru.t1.ktubaltseva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @NotNull
    Optional<UserDTO> findByLogin(@NotNull final String login);

    @NotNull
    Optional<UserDTO> findByEmail(@NotNull final String Email);

    void deleteByLogin(@NotNull final String login);

    void deleteByEmail(@NotNull final String email);

    Boolean existsByLogin(@NotNull final String login);

    Boolean existsByEmail(@NotNull final String email);

}
