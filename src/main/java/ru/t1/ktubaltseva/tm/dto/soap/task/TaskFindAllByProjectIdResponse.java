package ru.t1.ktubaltseva.tm.dto.soap.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskFindAllByProjectIdResponse")
public class TaskFindAllByProjectIdResponse {

    protected List<TaskDTO> task;

    public TaskFindAllByProjectIdResponse(@NotNull final List<TaskDTO> task) {
        this.task = task;
    }

}