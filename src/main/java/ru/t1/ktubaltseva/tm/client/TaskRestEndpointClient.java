package ru.t1.ktubaltseva.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.util.List;

public interface TaskRestEndpointClient {

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, "http://localhost:8080/api/tasks");
    }

    @PutMapping("/create")
    TaskDTO create() throws AbstractException;

    @PutMapping("/add")
    TaskDTO add(@RequestBody @NotNull final TaskDTO task) throws AbstractException;

    @DeleteMapping("/delete/{id}")
    void deleteById(@PathVariable("id") @NotNull final String id) throws AbstractException;

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") @NotNull final String id) throws AbstractException;

    @GetMapping("/findById/{id}")
    TaskDTO findById(@PathVariable("id") @NotNull final String id) throws AbstractException;

    @PostMapping("/update/{id}")
    TaskDTO update(@RequestBody @NotNull final TaskDTO task) throws AbstractException;

    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @DeleteMapping("/clear")
    void clear();

    @GetMapping("/count")
    long count();

}
