package ru.t1.ktubaltseva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.dto.ITaskDTOService;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.repository.dto.TaskDTORepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTOService extends AbstractDTOService<TaskDTO, TaskDTORepository> implements ITaskDTOService {

    @NotNull
    @Autowired
    private TaskDTORepository repository;

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public TaskDTO findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<TaskDTO> optionalM = repository.findByUserIdAndId(userId, id);
        return optionalM.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public long count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        return repository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void delete(@Nullable final String userId, @Nullable final TaskDTO model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        repository.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new EntityNotFoundException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO update(
            @Nullable final String userId,
            @Nullable final TaskDTO model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        if (!repository.existsByUserIdAndId(userId, model.getId())) throw new EntityNotFoundException();
        return repository.saveAndFlush(model);
    }

    @Override
    public @NotNull TaskDTO create() throws EntityNotFoundException {
        return add(new TaskDTO());
    }

    @Override
    public @NotNull List<TaskDTO> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    public @NotNull TaskDTO create(@Nullable final String userId) throws EntityNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO add(@Nullable final String userId, @Nullable final TaskDTO model) throws EntityNotFoundException, UserNotFoundException {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        return repository.saveAndFlush(model);
    }

}
