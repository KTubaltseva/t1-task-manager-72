package ru.t1.ktubaltseva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.rest.IAuthRestEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/auth")
public class AuthRestEndpoint implements IAuthRestEndpoint {

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IUserDTOService userDTOService;

    @PostMapping(value = "/login")
    public boolean login(
            @RequestParam("username") @NotNull final String username,
            @RequestParam("password") @NotNull final String password
    ) {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(username, password);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authentication.isAuthenticated();
    }

    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public boolean logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return true;
    }

    @NotNull
    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({"ROLE_ADMIN", "ROLE_USUAL"})
    public UserDTO profile() throws AbstractException {
        @NotNull final SecurityContext context = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = context.getAuthentication();
        @NotNull final String login = authentication.getName();
        return userDTOService.findByLogin(login);
    }

}

