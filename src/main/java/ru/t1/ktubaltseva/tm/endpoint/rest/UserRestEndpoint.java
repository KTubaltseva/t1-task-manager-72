package ru.t1.ktubaltseva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.rest.IUserRestEndpoint;
import ru.t1.ktubaltseva.tm.api.service.dto.IUserDTOService;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/users")
public class UserRestEndpoint implements IUserRestEndpoint {

    @NotNull
    @Autowired
    private IUserDTOService service;

    @Override
    @Secured({"ROLE_ADMIN"})
    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    public long count() throws AbstractException {
        return service.count();
    }

    @Override
    @Secured({"ROLE_ADMIN"})
    @DeleteMapping(value = "/clear", produces = APPLICATION_JSON_VALUE)
    public void clear() throws AbstractException {
        service.clear();
    }

    @Override
    @Secured({"ROLE_ADMIN"})
    @DeleteMapping(value = "/delete/{id}", produces = APPLICATION_JSON_VALUE)
    public void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @Secured({"ROLE_ADMIN"})
    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    public boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @Secured({"ROLE_ADMIN"})
    @GetMapping(value = "/findAll", produces = APPLICATION_JSON_VALUE)
    public List<UserDTO> findAll() throws AbstractException {
        return service.findAll();
    }

    @NotNull
    @Override
    @Secured({"ROLE_ADMIN"})
    @GetMapping(value = "/find/{id}", produces = APPLICATION_JSON_VALUE)
    public UserDTO findById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @Secured({"ROLE_ADMIN"})
    @PutMapping(value = "/add", produces = APPLICATION_JSON_VALUE)
    public UserDTO add(
            @RequestBody @NotNull final UserDTO user
    ) throws EntityNotFoundException {
        return service.add(user);
    }

    @NotNull
    @Override
    @Secured({"ROLE_ADMIN"})
    @PostMapping(value = "/update", produces = APPLICATION_JSON_VALUE)
    public UserDTO update(
            @RequestBody @NotNull final UserDTO user
    ) throws EntityNotFoundException {
        return service.add(user);
    }

}
